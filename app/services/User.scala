package services

import play.api.libs.json.{Json, JsObject, JsArray}
import scala.concurrent.ExecutionContext.Implicits.global 
import scala.concurrent.Future

object User {

	def get(id: Long): Future[JsObject] = {
    val uFuture = Connector.getUser(id)
    val pFuture = Connector.getPosts(id)
    for {
      uResponse <- uFuture
      pResponse <- pFuture
      user = uResponse.json.as[JsObject]
      posts = pResponse.json.as[JsArray]
    } yield (user ++ Json.obj("posts" -> posts))
	}
	
}
