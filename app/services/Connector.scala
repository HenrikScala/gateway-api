package services

import play.api.libs.ws.{WS, Response}
import scala.concurrent.Future

object Connector {

  val server = "http://jsonplaceholder.typicode.com"
    
	def getUser(id: Long): Future[Response] = 
    WS.url(s"$server/users/$id")
    .get()
	
  def getPosts(userId: Long): Future[Response] = 
    WS.url(s"$server/posts")
    .withQueryString("userId" -> userId.toString())
    .get()
  
}
