package controllers

import play.api.mvc._

import services.User
import scala.concurrent.ExecutionContext.Implicits.global 

object Users extends Controller {

  def get(id: Long) = Action.async {
    User.get(id).map{ Ok(_) }
  }

}